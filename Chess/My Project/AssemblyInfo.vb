﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Chess")> 
<Assembly: AssemblyDescription("Chess By KAREEM ADEL")> 
<Assembly: AssemblyCompany("KAREEM ADEL")> 
<Assembly: AssemblyProduct("Chess")> 
<Assembly: AssemblyCopyright("Copyright © KAREEM ADEL  2011")> 
<Assembly: AssemblyTrademark("Chess")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("38e9dd08-a92b-4119-9122-91ecc06e7c8d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en")> 