﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.location_indicator = New System.Windows.Forms.PictureBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.rook2_white = New System.Windows.Forms.PictureBox
        Me.knight2_white = New System.Windows.Forms.PictureBox
        Me.bishop2_white = New System.Windows.Forms.PictureBox
        Me.queen_white = New System.Windows.Forms.PictureBox
        Me.king_white = New System.Windows.Forms.PictureBox
        Me.bishop1_white = New System.Windows.Forms.PictureBox
        Me.knight1_white = New System.Windows.Forms.PictureBox
        Me.rook1_white = New System.Windows.Forms.PictureBox
        Me.soldier1_white = New System.Windows.Forms.PictureBox
        Me.soldier2_white = New System.Windows.Forms.PictureBox
        Me.soldier3_white = New System.Windows.Forms.PictureBox
        Me.soldier4_white = New System.Windows.Forms.PictureBox
        Me.soldier5_white = New System.Windows.Forms.PictureBox
        Me.soldier6_white = New System.Windows.Forms.PictureBox
        Me.soldier7_white = New System.Windows.Forms.PictureBox
        Me.soldier8_white = New System.Windows.Forms.PictureBox
        Me.rook1_black = New System.Windows.Forms.PictureBox
        Me.knight1_black = New System.Windows.Forms.PictureBox
        Me.bishop1_black = New System.Windows.Forms.PictureBox
        Me.king_black = New System.Windows.Forms.PictureBox
        Me.queen_black = New System.Windows.Forms.PictureBox
        Me.bishop2_black = New System.Windows.Forms.PictureBox
        Me.knight2_black = New System.Windows.Forms.PictureBox
        Me.rook2_black = New System.Windows.Forms.PictureBox
        Me.soldier1_black = New System.Windows.Forms.PictureBox
        Me.soldier2_black = New System.Windows.Forms.PictureBox
        Me.soldier3_black = New System.Windows.Forms.PictureBox
        Me.soldier4_black = New System.Windows.Forms.PictureBox
        Me.soldier5_black = New System.Windows.Forms.PictureBox
        Me.soldier6_black = New System.Windows.Forms.PictureBox
        Me.soldier7_black = New System.Windows.Forms.PictureBox
        Me.soldier8_black = New System.Windows.Forms.PictureBox
        Me.floor1 = New System.Windows.Forms.PictureBox
        Me.floor2 = New System.Windows.Forms.PictureBox
        Me.floor3 = New System.Windows.Forms.PictureBox
        Me.floor4 = New System.Windows.Forms.PictureBox
        Me.floor5 = New System.Windows.Forms.PictureBox
        Me.floor6 = New System.Windows.Forms.PictureBox
        Me.floor7 = New System.Windows.Forms.PictureBox
        Me.floor8 = New System.Windows.Forms.PictureBox
        Me.floor9 = New System.Windows.Forms.PictureBox
        Me.floor10 = New System.Windows.Forms.PictureBox
        Me.floor11 = New System.Windows.Forms.PictureBox
        Me.floor12 = New System.Windows.Forms.PictureBox
        Me.floor13 = New System.Windows.Forms.PictureBox
        Me.floor14 = New System.Windows.Forms.PictureBox
        Me.floor15 = New System.Windows.Forms.PictureBox
        Me.floor16 = New System.Windows.Forms.PictureBox
        Me.floor17 = New System.Windows.Forms.PictureBox
        Me.floor18 = New System.Windows.Forms.PictureBox
        Me.floor19 = New System.Windows.Forms.PictureBox
        Me.floor20 = New System.Windows.Forms.PictureBox
        Me.floor21 = New System.Windows.Forms.PictureBox
        Me.floor22 = New System.Windows.Forms.PictureBox
        Me.floor23 = New System.Windows.Forms.PictureBox
        Me.floor24 = New System.Windows.Forms.PictureBox
        Me.floor25 = New System.Windows.Forms.PictureBox
        Me.floor26 = New System.Windows.Forms.PictureBox
        Me.floor27 = New System.Windows.Forms.PictureBox
        Me.floor28 = New System.Windows.Forms.PictureBox
        Me.floor29 = New System.Windows.Forms.PictureBox
        Me.floor30 = New System.Windows.Forms.PictureBox
        Me.floor31 = New System.Windows.Forms.PictureBox
        Me.floor32 = New System.Windows.Forms.PictureBox
        Me.floor33 = New System.Windows.Forms.PictureBox
        Me.floor34 = New System.Windows.Forms.PictureBox
        Me.floor35 = New System.Windows.Forms.PictureBox
        Me.floor36 = New System.Windows.Forms.PictureBox
        Me.floor37 = New System.Windows.Forms.PictureBox
        Me.floor38 = New System.Windows.Forms.PictureBox
        Me.floor39 = New System.Windows.Forms.PictureBox
        Me.floor40 = New System.Windows.Forms.PictureBox
        Me.floor48 = New System.Windows.Forms.PictureBox
        Me.floor47 = New System.Windows.Forms.PictureBox
        Me.floor46 = New System.Windows.Forms.PictureBox
        Me.floor45 = New System.Windows.Forms.PictureBox
        Me.floor44 = New System.Windows.Forms.PictureBox
        Me.floor43 = New System.Windows.Forms.PictureBox
        Me.floor42 = New System.Windows.Forms.PictureBox
        Me.floor41 = New System.Windows.Forms.PictureBox
        Me.floor56 = New System.Windows.Forms.PictureBox
        Me.floor55 = New System.Windows.Forms.PictureBox
        Me.floor54 = New System.Windows.Forms.PictureBox
        Me.floor53 = New System.Windows.Forms.PictureBox
        Me.floor52 = New System.Windows.Forms.PictureBox
        Me.floor51 = New System.Windows.Forms.PictureBox
        Me.floor50 = New System.Windows.Forms.PictureBox
        Me.floor49 = New System.Windows.Forms.PictureBox
        Me.floor64 = New System.Windows.Forms.PictureBox
        Me.floor63 = New System.Windows.Forms.PictureBox
        Me.floor62 = New System.Windows.Forms.PictureBox
        Me.floor61 = New System.Windows.Forms.PictureBox
        Me.floor60 = New System.Windows.Forms.PictureBox
        Me.floor59 = New System.Windows.Forms.PictureBox
        Me.floor58 = New System.Windows.Forms.PictureBox
        Me.floor57 = New System.Windows.Forms.PictureBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer4 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer5 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer6 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer7 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer8 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer9 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer10 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer11 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer12 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer13 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer14 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer15 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer16 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer17 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer18 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer19 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer20 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer21 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer22 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer23 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer24 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer25 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer26 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer27 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer28 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer29 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer30 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer31 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer32 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Button10 = New System.Windows.Forms.Button
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Timer33 = New System.Windows.Forms.Timer(Me.components)
        Me.Button11 = New System.Windows.Forms.Button
        Me.Timer34 = New System.Windows.Forms.Timer(Me.components)
        Me.Button12 = New System.Windows.Forms.Button
        CType(Me.location_indicator, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.rook2_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.knight2_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bishop2_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.queen_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.king_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bishop1_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.knight1_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rook1_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier1_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier2_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier3_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier4_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier5_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier6_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier7_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier8_white, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rook1_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.knight1_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bishop1_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.king_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.queen_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bishop2_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.knight2_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rook2_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier1_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier2_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier3_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier4_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier5_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier6_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier7_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.soldier8_black, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.floor57, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'location_indicator
        '
        Me.location_indicator.BackColor = System.Drawing.Color.Transparent
        Me.location_indicator.Image = Global.Chess.My.Resources.Resources.Chess_pic
        Me.location_indicator.Location = New System.Drawing.Point(0, 0)
        Me.location_indicator.Name = "location_indicator"
        Me.location_indicator.Size = New System.Drawing.Size(450, 450)
        Me.location_indicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.location_indicator.TabIndex = 0
        Me.location_indicator.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(6, 119)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(198, 20)
        Me.TextBox1.TabIndex = 3
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox2.Image = Global.Chess.My.Resources.Resources.wood
        Me.PictureBox2.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(694, 472)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 4
        Me.PictureBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(464, 308)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(210, 142)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Chat"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 12
        Me.ListBox1.Location = New System.Drawing.Point(6, 16)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(198, 100)
        Me.ListBox1.TabIndex = 122
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Transparent
        Me.Button3.Location = New System.Drawing.Point(8, 13)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(72, 23)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "Black"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(547, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "VS"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Crimson
        Me.Label2.Location = New System.Drawing.Point(598, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 20)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "......."
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.ForestGreen
        Me.Label3.Location = New System.Drawing.Point(466, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 20)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "......"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(498, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Waiting For A game"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'rook2_white
        '
        Me.rook2_white.BackColor = System.Drawing.Color.Transparent
        Me.rook2_white.Image = CType(resources.GetObject("rook2_white.Image"), System.Drawing.Image)
        Me.rook2_white.Location = New System.Drawing.Point(1, 2)
        Me.rook2_white.Name = "rook2_white"
        Me.rook2_white.Size = New System.Drawing.Size(55, 53)
        Me.rook2_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.rook2_white.TabIndex = 4
        Me.rook2_white.TabStop = False
        '
        'knight2_white
        '
        Me.knight2_white.BackColor = System.Drawing.Color.Transparent
        Me.knight2_white.Image = CType(resources.GetObject("knight2_white.Image"), System.Drawing.Image)
        Me.knight2_white.Location = New System.Drawing.Point(57, 2)
        Me.knight2_white.Name = "knight2_white"
        Me.knight2_white.Size = New System.Drawing.Size(55, 53)
        Me.knight2_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.knight2_white.TabIndex = 13
        Me.knight2_white.TabStop = False
        '
        'bishop2_white
        '
        Me.bishop2_white.BackColor = System.Drawing.Color.Transparent
        Me.bishop2_white.Image = CType(resources.GetObject("bishop2_white.Image"), System.Drawing.Image)
        Me.bishop2_white.Location = New System.Drawing.Point(113, 2)
        Me.bishop2_white.Name = "bishop2_white"
        Me.bishop2_white.Size = New System.Drawing.Size(55, 53)
        Me.bishop2_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.bishop2_white.TabIndex = 14
        Me.bishop2_white.TabStop = False
        '
        'queen_white
        '
        Me.queen_white.BackColor = System.Drawing.Color.Transparent
        Me.queen_white.Image = Global.Chess.My.Resources.Resources.wk
        Me.queen_white.Location = New System.Drawing.Point(170, 2)
        Me.queen_white.Name = "queen_white"
        Me.queen_white.Size = New System.Drawing.Size(53, 53)
        Me.queen_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.queen_white.TabIndex = 15
        Me.queen_white.TabStop = False
        '
        'king_white
        '
        Me.king_white.BackColor = System.Drawing.Color.Transparent
        Me.king_white.Image = Global.Chess.My.Resources.Resources.wq
        Me.king_white.Location = New System.Drawing.Point(224, 3)
        Me.king_white.Name = "king_white"
        Me.king_white.Size = New System.Drawing.Size(55, 53)
        Me.king_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.king_white.TabIndex = 16
        Me.king_white.TabStop = False
        '
        'bishop1_white
        '
        Me.bishop1_white.BackColor = System.Drawing.Color.Transparent
        Me.bishop1_white.Image = CType(resources.GetObject("bishop1_white.Image"), System.Drawing.Image)
        Me.bishop1_white.Location = New System.Drawing.Point(280, 3)
        Me.bishop1_white.Name = "bishop1_white"
        Me.bishop1_white.Size = New System.Drawing.Size(55, 53)
        Me.bishop1_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.bishop1_white.TabIndex = 17
        Me.bishop1_white.TabStop = False
        '
        'knight1_white
        '
        Me.knight1_white.BackColor = System.Drawing.Color.Transparent
        Me.knight1_white.Image = CType(resources.GetObject("knight1_white.Image"), System.Drawing.Image)
        Me.knight1_white.Location = New System.Drawing.Point(336, 3)
        Me.knight1_white.Name = "knight1_white"
        Me.knight1_white.Size = New System.Drawing.Size(55, 53)
        Me.knight1_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.knight1_white.TabIndex = 18
        Me.knight1_white.TabStop = False
        '
        'rook1_white
        '
        Me.rook1_white.BackColor = System.Drawing.Color.Transparent
        Me.rook1_white.Image = CType(resources.GetObject("rook1_white.Image"), System.Drawing.Image)
        Me.rook1_white.Location = New System.Drawing.Point(393, 3)
        Me.rook1_white.Name = "rook1_white"
        Me.rook1_white.Size = New System.Drawing.Size(55, 53)
        Me.rook1_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.rook1_white.TabIndex = 19
        Me.rook1_white.TabStop = False
        '
        'soldier1_white
        '
        Me.soldier1_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier1_white.Image = Global.Chess.My.Resources.Resources.wp
        Me.soldier1_white.Location = New System.Drawing.Point(393, 58)
        Me.soldier1_white.Name = "soldier1_white"
        Me.soldier1_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier1_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier1_white.TabIndex = 27
        Me.soldier1_white.TabStop = False
        '
        'soldier2_white
        '
        Me.soldier2_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier2_white.Image = Global.Chess.My.Resources.Resources.wp
        Me.soldier2_white.Location = New System.Drawing.Point(336, 59)
        Me.soldier2_white.Name = "soldier2_white"
        Me.soldier2_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier2_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier2_white.TabIndex = 26
        Me.soldier2_white.TabStop = False
        '
        'soldier3_white
        '
        Me.soldier3_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier3_white.Image = CType(resources.GetObject("soldier3_white.Image"), System.Drawing.Image)
        Me.soldier3_white.Location = New System.Drawing.Point(280, 59)
        Me.soldier3_white.Name = "soldier3_white"
        Me.soldier3_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier3_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier3_white.TabIndex = 25
        Me.soldier3_white.TabStop = False
        '
        'soldier4_white
        '
        Me.soldier4_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier4_white.Image = CType(resources.GetObject("soldier4_white.Image"), System.Drawing.Image)
        Me.soldier4_white.Location = New System.Drawing.Point(225, 58)
        Me.soldier4_white.Name = "soldier4_white"
        Me.soldier4_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier4_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier4_white.TabIndex = 24
        Me.soldier4_white.TabStop = False
        '
        'soldier5_white
        '
        Me.soldier5_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier5_white.Image = CType(resources.GetObject("soldier5_white.Image"), System.Drawing.Image)
        Me.soldier5_white.Location = New System.Drawing.Point(172, 58)
        Me.soldier5_white.Name = "soldier5_white"
        Me.soldier5_white.Size = New System.Drawing.Size(53, 53)
        Me.soldier5_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier5_white.TabIndex = 23
        Me.soldier5_white.TabStop = False
        '
        'soldier6_white
        '
        Me.soldier6_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier6_white.Image = CType(resources.GetObject("soldier6_white.Image"), System.Drawing.Image)
        Me.soldier6_white.Location = New System.Drawing.Point(116, 57)
        Me.soldier6_white.Name = "soldier6_white"
        Me.soldier6_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier6_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier6_white.TabIndex = 22
        Me.soldier6_white.TabStop = False
        '
        'soldier7_white
        '
        Me.soldier7_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier7_white.Image = CType(resources.GetObject("soldier7_white.Image"), System.Drawing.Image)
        Me.soldier7_white.Location = New System.Drawing.Point(59, 58)
        Me.soldier7_white.Name = "soldier7_white"
        Me.soldier7_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier7_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier7_white.TabIndex = 21
        Me.soldier7_white.TabStop = False
        '
        'soldier8_white
        '
        Me.soldier8_white.BackColor = System.Drawing.Color.Transparent
        Me.soldier8_white.Image = CType(resources.GetObject("soldier8_white.Image"), System.Drawing.Image)
        Me.soldier8_white.Location = New System.Drawing.Point(2, 58)
        Me.soldier8_white.Name = "soldier8_white"
        Me.soldier8_white.Size = New System.Drawing.Size(55, 53)
        Me.soldier8_white.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier8_white.TabIndex = 20
        Me.soldier8_white.TabStop = False
        '
        'rook1_black
        '
        Me.rook1_black.BackColor = System.Drawing.Color.Transparent
        Me.rook1_black.Image = Global.Chess.My.Resources.Resources.br
        Me.rook1_black.Location = New System.Drawing.Point(393, 394)
        Me.rook1_black.Name = "rook1_black"
        Me.rook1_black.Size = New System.Drawing.Size(55, 53)
        Me.rook1_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.rook1_black.TabIndex = 35
        Me.rook1_black.TabStop = False
        '
        'knight1_black
        '
        Me.knight1_black.BackColor = System.Drawing.Color.Transparent
        Me.knight1_black.Image = CType(resources.GetObject("knight1_black.Image"), System.Drawing.Image)
        Me.knight1_black.Location = New System.Drawing.Point(337, 394)
        Me.knight1_black.Name = "knight1_black"
        Me.knight1_black.Size = New System.Drawing.Size(55, 53)
        Me.knight1_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.knight1_black.TabIndex = 34
        Me.knight1_black.TabStop = False
        '
        'bishop1_black
        '
        Me.bishop1_black.BackColor = System.Drawing.Color.Transparent
        Me.bishop1_black.Image = CType(resources.GetObject("bishop1_black.Image"), System.Drawing.Image)
        Me.bishop1_black.Location = New System.Drawing.Point(281, 394)
        Me.bishop1_black.Name = "bishop1_black"
        Me.bishop1_black.Size = New System.Drawing.Size(55, 53)
        Me.bishop1_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.bishop1_black.TabIndex = 33
        Me.bishop1_black.TabStop = False
        '
        'king_black
        '
        Me.king_black.BackColor = System.Drawing.Color.Transparent
        Me.king_black.Image = Global.Chess.My.Resources.Resources.bq
        Me.king_black.Location = New System.Drawing.Point(225, 394)
        Me.king_black.Name = "king_black"
        Me.king_black.Size = New System.Drawing.Size(55, 53)
        Me.king_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.king_black.TabIndex = 32
        Me.king_black.TabStop = False
        '
        'queen_black
        '
        Me.queen_black.BackColor = System.Drawing.Color.Transparent
        Me.queen_black.Image = Global.Chess.My.Resources.Resources.bk
        Me.queen_black.Location = New System.Drawing.Point(172, 395)
        Me.queen_black.Name = "queen_black"
        Me.queen_black.Size = New System.Drawing.Size(53, 53)
        Me.queen_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.queen_black.TabIndex = 31
        Me.queen_black.TabStop = False
        '
        'bishop2_black
        '
        Me.bishop2_black.BackColor = System.Drawing.Color.Transparent
        Me.bishop2_black.Image = CType(resources.GetObject("bishop2_black.Image"), System.Drawing.Image)
        Me.bishop2_black.Location = New System.Drawing.Point(116, 394)
        Me.bishop2_black.Name = "bishop2_black"
        Me.bishop2_black.Size = New System.Drawing.Size(55, 53)
        Me.bishop2_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.bishop2_black.TabIndex = 30
        Me.bishop2_black.TabStop = False
        '
        'knight2_black
        '
        Me.knight2_black.BackColor = System.Drawing.Color.Transparent
        Me.knight2_black.Image = CType(resources.GetObject("knight2_black.Image"), System.Drawing.Image)
        Me.knight2_black.Location = New System.Drawing.Point(59, 394)
        Me.knight2_black.Name = "knight2_black"
        Me.knight2_black.Size = New System.Drawing.Size(55, 53)
        Me.knight2_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.knight2_black.TabIndex = 29
        Me.knight2_black.TabStop = False
        '
        'rook2_black
        '
        Me.rook2_black.BackColor = System.Drawing.Color.Transparent
        Me.rook2_black.Image = Global.Chess.My.Resources.Resources.br
        Me.rook2_black.Location = New System.Drawing.Point(2, 394)
        Me.rook2_black.Name = "rook2_black"
        Me.rook2_black.Size = New System.Drawing.Size(55, 53)
        Me.rook2_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.rook2_black.TabIndex = 28
        Me.rook2_black.TabStop = False
        '
        'soldier1_black
        '
        Me.soldier1_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier1_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier1_black.Location = New System.Drawing.Point(393, 339)
        Me.soldier1_black.Name = "soldier1_black"
        Me.soldier1_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier1_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier1_black.TabIndex = 43
        Me.soldier1_black.TabStop = False
        '
        'soldier2_black
        '
        Me.soldier2_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier2_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier2_black.Location = New System.Drawing.Point(337, 339)
        Me.soldier2_black.Name = "soldier2_black"
        Me.soldier2_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier2_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier2_black.TabIndex = 42
        Me.soldier2_black.TabStop = False
        '
        'soldier3_black
        '
        Me.soldier3_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier3_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier3_black.Location = New System.Drawing.Point(281, 339)
        Me.soldier3_black.Name = "soldier3_black"
        Me.soldier3_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier3_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier3_black.TabIndex = 41
        Me.soldier3_black.TabStop = False
        '
        'soldier4_black
        '
        Me.soldier4_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier4_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier4_black.Location = New System.Drawing.Point(225, 339)
        Me.soldier4_black.Name = "soldier4_black"
        Me.soldier4_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier4_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier4_black.TabIndex = 40
        Me.soldier4_black.TabStop = False
        '
        'soldier5_black
        '
        Me.soldier5_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier5_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier5_black.Location = New System.Drawing.Point(171, 339)
        Me.soldier5_black.Name = "soldier5_black"
        Me.soldier5_black.Size = New System.Drawing.Size(53, 53)
        Me.soldier5_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier5_black.TabIndex = 39
        Me.soldier5_black.TabStop = False
        '
        'soldier6_black
        '
        Me.soldier6_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier6_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier6_black.Location = New System.Drawing.Point(115, 338)
        Me.soldier6_black.Name = "soldier6_black"
        Me.soldier6_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier6_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier6_black.TabIndex = 38
        Me.soldier6_black.TabStop = False
        '
        'soldier7_black
        '
        Me.soldier7_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier7_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier7_black.Location = New System.Drawing.Point(59, 338)
        Me.soldier7_black.Name = "soldier7_black"
        Me.soldier7_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier7_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier7_black.TabIndex = 37
        Me.soldier7_black.TabStop = False
        '
        'soldier8_black
        '
        Me.soldier8_black.BackColor = System.Drawing.Color.Transparent
        Me.soldier8_black.Image = Global.Chess.My.Resources.Resources.bp
        Me.soldier8_black.Location = New System.Drawing.Point(2, 338)
        Me.soldier8_black.Name = "soldier8_black"
        Me.soldier8_black.Size = New System.Drawing.Size(55, 53)
        Me.soldier8_black.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.soldier8_black.TabIndex = 36
        Me.soldier8_black.TabStop = False
        '
        'floor1
        '
        Me.floor1.BackColor = System.Drawing.Color.Transparent
        Me.floor1.Location = New System.Drawing.Point(10, 8)
        Me.floor1.Name = "floor1"
        Me.floor1.Size = New System.Drawing.Size(40, 40)
        Me.floor1.TabIndex = 44
        Me.floor1.TabStop = False
        '
        'floor2
        '
        Me.floor2.BackColor = System.Drawing.Color.Transparent
        Me.floor2.Location = New System.Drawing.Point(67, 8)
        Me.floor2.Name = "floor2"
        Me.floor2.Size = New System.Drawing.Size(40, 40)
        Me.floor2.TabIndex = 45
        Me.floor2.TabStop = False
        '
        'floor3
        '
        Me.floor3.BackColor = System.Drawing.Color.Transparent
        Me.floor3.Location = New System.Drawing.Point(124, 8)
        Me.floor3.Name = "floor3"
        Me.floor3.Size = New System.Drawing.Size(40, 40)
        Me.floor3.TabIndex = 46
        Me.floor3.TabStop = False
        '
        'floor4
        '
        Me.floor4.BackColor = System.Drawing.Color.Transparent
        Me.floor4.Location = New System.Drawing.Point(179, 9)
        Me.floor4.Name = "floor4"
        Me.floor4.Size = New System.Drawing.Size(40, 40)
        Me.floor4.TabIndex = 47
        Me.floor4.TabStop = False
        '
        'floor5
        '
        Me.floor5.BackColor = System.Drawing.Color.Transparent
        Me.floor5.Location = New System.Drawing.Point(232, 9)
        Me.floor5.Name = "floor5"
        Me.floor5.Size = New System.Drawing.Size(40, 40)
        Me.floor5.TabIndex = 48
        Me.floor5.TabStop = False
        '
        'floor6
        '
        Me.floor6.BackColor = System.Drawing.Color.Transparent
        Me.floor6.Location = New System.Drawing.Point(287, 9)
        Me.floor6.Name = "floor6"
        Me.floor6.Size = New System.Drawing.Size(40, 40)
        Me.floor6.TabIndex = 49
        Me.floor6.TabStop = False
        '
        'floor7
        '
        Me.floor7.BackColor = System.Drawing.Color.Transparent
        Me.floor7.Location = New System.Drawing.Point(344, 9)
        Me.floor7.Name = "floor7"
        Me.floor7.Size = New System.Drawing.Size(40, 40)
        Me.floor7.TabIndex = 50
        Me.floor7.TabStop = False
        '
        'floor8
        '
        Me.floor8.BackColor = System.Drawing.Color.Transparent
        Me.floor8.Location = New System.Drawing.Point(401, 9)
        Me.floor8.Name = "floor8"
        Me.floor8.Size = New System.Drawing.Size(40, 40)
        Me.floor8.TabIndex = 51
        Me.floor8.TabStop = False
        '
        'floor9
        '
        Me.floor9.BackColor = System.Drawing.Color.Transparent
        Me.floor9.Location = New System.Drawing.Point(10, 64)
        Me.floor9.Name = "floor9"
        Me.floor9.Size = New System.Drawing.Size(40, 40)
        Me.floor9.TabIndex = 52
        Me.floor9.TabStop = False
        '
        'floor10
        '
        Me.floor10.BackColor = System.Drawing.Color.Transparent
        Me.floor10.Location = New System.Drawing.Point(67, 64)
        Me.floor10.Name = "floor10"
        Me.floor10.Size = New System.Drawing.Size(40, 40)
        Me.floor10.TabIndex = 53
        Me.floor10.TabStop = False
        '
        'floor11
        '
        Me.floor11.BackColor = System.Drawing.Color.Transparent
        Me.floor11.Location = New System.Drawing.Point(124, 64)
        Me.floor11.Name = "floor11"
        Me.floor11.Size = New System.Drawing.Size(40, 40)
        Me.floor11.TabIndex = 54
        Me.floor11.TabStop = False
        '
        'floor12
        '
        Me.floor12.BackColor = System.Drawing.Color.Transparent
        Me.floor12.Location = New System.Drawing.Point(179, 65)
        Me.floor12.Name = "floor12"
        Me.floor12.Size = New System.Drawing.Size(40, 40)
        Me.floor12.TabIndex = 55
        Me.floor12.TabStop = False
        '
        'floor13
        '
        Me.floor13.BackColor = System.Drawing.Color.Transparent
        Me.floor13.Location = New System.Drawing.Point(232, 65)
        Me.floor13.Name = "floor13"
        Me.floor13.Size = New System.Drawing.Size(40, 40)
        Me.floor13.TabIndex = 56
        Me.floor13.TabStop = False
        '
        'floor14
        '
        Me.floor14.BackColor = System.Drawing.Color.Transparent
        Me.floor14.Location = New System.Drawing.Point(287, 65)
        Me.floor14.Name = "floor14"
        Me.floor14.Size = New System.Drawing.Size(40, 40)
        Me.floor14.TabIndex = 57
        Me.floor14.TabStop = False
        '
        'floor15
        '
        Me.floor15.BackColor = System.Drawing.Color.Transparent
        Me.floor15.Location = New System.Drawing.Point(344, 65)
        Me.floor15.Name = "floor15"
        Me.floor15.Size = New System.Drawing.Size(40, 40)
        Me.floor15.TabIndex = 58
        Me.floor15.TabStop = False
        '
        'floor16
        '
        Me.floor16.BackColor = System.Drawing.Color.Transparent
        Me.floor16.Location = New System.Drawing.Point(401, 64)
        Me.floor16.Name = "floor16"
        Me.floor16.Size = New System.Drawing.Size(40, 40)
        Me.floor16.TabIndex = 59
        Me.floor16.TabStop = False
        '
        'floor17
        '
        Me.floor17.BackColor = System.Drawing.Color.Transparent
        Me.floor17.Location = New System.Drawing.Point(10, 121)
        Me.floor17.Name = "floor17"
        Me.floor17.Size = New System.Drawing.Size(40, 40)
        Me.floor17.TabIndex = 60
        Me.floor17.TabStop = False
        '
        'floor18
        '
        Me.floor18.BackColor = System.Drawing.Color.Transparent
        Me.floor18.Location = New System.Drawing.Point(67, 120)
        Me.floor18.Name = "floor18"
        Me.floor18.Size = New System.Drawing.Size(40, 40)
        Me.floor18.TabIndex = 61
        Me.floor18.TabStop = False
        '
        'floor19
        '
        Me.floor19.BackColor = System.Drawing.Color.Transparent
        Me.floor19.Location = New System.Drawing.Point(124, 120)
        Me.floor19.Name = "floor19"
        Me.floor19.Size = New System.Drawing.Size(40, 40)
        Me.floor19.TabIndex = 62
        Me.floor19.TabStop = False
        '
        'floor20
        '
        Me.floor20.BackColor = System.Drawing.Color.Transparent
        Me.floor20.Location = New System.Drawing.Point(179, 121)
        Me.floor20.Name = "floor20"
        Me.floor20.Size = New System.Drawing.Size(40, 40)
        Me.floor20.TabIndex = 63
        Me.floor20.TabStop = False
        '
        'floor21
        '
        Me.floor21.BackColor = System.Drawing.Color.Transparent
        Me.floor21.Location = New System.Drawing.Point(232, 121)
        Me.floor21.Name = "floor21"
        Me.floor21.Size = New System.Drawing.Size(40, 40)
        Me.floor21.TabIndex = 64
        Me.floor21.TabStop = False
        '
        'floor22
        '
        Me.floor22.BackColor = System.Drawing.Color.Transparent
        Me.floor22.Location = New System.Drawing.Point(287, 122)
        Me.floor22.Name = "floor22"
        Me.floor22.Size = New System.Drawing.Size(40, 40)
        Me.floor22.TabIndex = 65
        Me.floor22.TabStop = False
        '
        'floor23
        '
        Me.floor23.BackColor = System.Drawing.Color.Transparent
        Me.floor23.Location = New System.Drawing.Point(344, 122)
        Me.floor23.Name = "floor23"
        Me.floor23.Size = New System.Drawing.Size(40, 40)
        Me.floor23.TabIndex = 66
        Me.floor23.TabStop = False
        '
        'floor24
        '
        Me.floor24.BackColor = System.Drawing.Color.Transparent
        Me.floor24.Location = New System.Drawing.Point(401, 122)
        Me.floor24.Name = "floor24"
        Me.floor24.Size = New System.Drawing.Size(40, 40)
        Me.floor24.TabIndex = 67
        Me.floor24.TabStop = False
        '
        'floor25
        '
        Me.floor25.BackColor = System.Drawing.Color.Transparent
        Me.floor25.Location = New System.Drawing.Point(10, 177)
        Me.floor25.Name = "floor25"
        Me.floor25.Size = New System.Drawing.Size(40, 40)
        Me.floor25.TabIndex = 68
        Me.floor25.TabStop = False
        '
        'floor26
        '
        Me.floor26.BackColor = System.Drawing.Color.Transparent
        Me.floor26.Location = New System.Drawing.Point(67, 176)
        Me.floor26.Name = "floor26"
        Me.floor26.Size = New System.Drawing.Size(40, 40)
        Me.floor26.TabIndex = 69
        Me.floor26.TabStop = False
        '
        'floor27
        '
        Me.floor27.BackColor = System.Drawing.Color.Transparent
        Me.floor27.Location = New System.Drawing.Point(124, 176)
        Me.floor27.Name = "floor27"
        Me.floor27.Size = New System.Drawing.Size(40, 40)
        Me.floor27.TabIndex = 70
        Me.floor27.TabStop = False
        '
        'floor28
        '
        Me.floor28.BackColor = System.Drawing.Color.Transparent
        Me.floor28.Location = New System.Drawing.Point(179, 177)
        Me.floor28.Name = "floor28"
        Me.floor28.Size = New System.Drawing.Size(40, 40)
        Me.floor28.TabIndex = 71
        Me.floor28.TabStop = False
        '
        'floor29
        '
        Me.floor29.BackColor = System.Drawing.Color.Transparent
        Me.floor29.Location = New System.Drawing.Point(233, 177)
        Me.floor29.Name = "floor29"
        Me.floor29.Size = New System.Drawing.Size(40, 40)
        Me.floor29.TabIndex = 72
        Me.floor29.TabStop = False
        '
        'floor30
        '
        Me.floor30.BackColor = System.Drawing.Color.Transparent
        Me.floor30.Location = New System.Drawing.Point(287, 178)
        Me.floor30.Name = "floor30"
        Me.floor30.Size = New System.Drawing.Size(40, 40)
        Me.floor30.TabIndex = 73
        Me.floor30.TabStop = False
        '
        'floor31
        '
        Me.floor31.BackColor = System.Drawing.Color.Transparent
        Me.floor31.Location = New System.Drawing.Point(344, 177)
        Me.floor31.Name = "floor31"
        Me.floor31.Size = New System.Drawing.Size(40, 40)
        Me.floor31.TabIndex = 74
        Me.floor31.TabStop = False
        '
        'floor32
        '
        Me.floor32.BackColor = System.Drawing.Color.Transparent
        Me.floor32.Location = New System.Drawing.Point(401, 176)
        Me.floor32.Name = "floor32"
        Me.floor32.Size = New System.Drawing.Size(40, 40)
        Me.floor32.TabIndex = 75
        Me.floor32.TabStop = False
        '
        'floor33
        '
        Me.floor33.BackColor = System.Drawing.Color.Transparent
        Me.floor33.Location = New System.Drawing.Point(10, 232)
        Me.floor33.Name = "floor33"
        Me.floor33.Size = New System.Drawing.Size(40, 40)
        Me.floor33.TabIndex = 76
        Me.floor33.TabStop = False
        '
        'floor34
        '
        Me.floor34.BackColor = System.Drawing.Color.Transparent
        Me.floor34.Location = New System.Drawing.Point(67, 232)
        Me.floor34.Name = "floor34"
        Me.floor34.Size = New System.Drawing.Size(40, 40)
        Me.floor34.TabIndex = 77
        Me.floor34.TabStop = False
        '
        'floor35
        '
        Me.floor35.BackColor = System.Drawing.Color.Transparent
        Me.floor35.Location = New System.Drawing.Point(124, 232)
        Me.floor35.Name = "floor35"
        Me.floor35.Size = New System.Drawing.Size(40, 40)
        Me.floor35.TabIndex = 78
        Me.floor35.TabStop = False
        '
        'floor36
        '
        Me.floor36.BackColor = System.Drawing.Color.Transparent
        Me.floor36.Location = New System.Drawing.Point(179, 232)
        Me.floor36.Name = "floor36"
        Me.floor36.Size = New System.Drawing.Size(40, 40)
        Me.floor36.TabIndex = 79
        Me.floor36.TabStop = False
        '
        'floor37
        '
        Me.floor37.BackColor = System.Drawing.Color.Transparent
        Me.floor37.Location = New System.Drawing.Point(233, 233)
        Me.floor37.Name = "floor37"
        Me.floor37.Size = New System.Drawing.Size(40, 40)
        Me.floor37.TabIndex = 80
        Me.floor37.TabStop = False
        '
        'floor38
        '
        Me.floor38.BackColor = System.Drawing.Color.Transparent
        Me.floor38.Location = New System.Drawing.Point(287, 233)
        Me.floor38.Name = "floor38"
        Me.floor38.Size = New System.Drawing.Size(40, 40)
        Me.floor38.TabIndex = 81
        Me.floor38.TabStop = False
        '
        'floor39
        '
        Me.floor39.BackColor = System.Drawing.Color.Transparent
        Me.floor39.Location = New System.Drawing.Point(344, 233)
        Me.floor39.Name = "floor39"
        Me.floor39.Size = New System.Drawing.Size(40, 40)
        Me.floor39.TabIndex = 82
        Me.floor39.TabStop = False
        '
        'floor40
        '
        Me.floor40.BackColor = System.Drawing.Color.Transparent
        Me.floor40.Location = New System.Drawing.Point(401, 234)
        Me.floor40.Name = "floor40"
        Me.floor40.Size = New System.Drawing.Size(40, 40)
        Me.floor40.TabIndex = 83
        Me.floor40.TabStop = False
        '
        'floor48
        '
        Me.floor48.BackColor = System.Drawing.Color.Transparent
        Me.floor48.Location = New System.Drawing.Point(401, 290)
        Me.floor48.Name = "floor48"
        Me.floor48.Size = New System.Drawing.Size(40, 40)
        Me.floor48.TabIndex = 91
        Me.floor48.TabStop = False
        '
        'floor47
        '
        Me.floor47.BackColor = System.Drawing.Color.Transparent
        Me.floor47.Location = New System.Drawing.Point(344, 290)
        Me.floor47.Name = "floor47"
        Me.floor47.Size = New System.Drawing.Size(40, 40)
        Me.floor47.TabIndex = 90
        Me.floor47.TabStop = False
        '
        'floor46
        '
        Me.floor46.BackColor = System.Drawing.Color.Transparent
        Me.floor46.Location = New System.Drawing.Point(287, 289)
        Me.floor46.Name = "floor46"
        Me.floor46.Size = New System.Drawing.Size(40, 40)
        Me.floor46.TabIndex = 89
        Me.floor46.TabStop = False
        '
        'floor45
        '
        Me.floor45.BackColor = System.Drawing.Color.Transparent
        Me.floor45.Location = New System.Drawing.Point(233, 289)
        Me.floor45.Name = "floor45"
        Me.floor45.Size = New System.Drawing.Size(40, 40)
        Me.floor45.TabIndex = 88
        Me.floor45.TabStop = False
        '
        'floor44
        '
        Me.floor44.BackColor = System.Drawing.Color.Transparent
        Me.floor44.Location = New System.Drawing.Point(179, 288)
        Me.floor44.Name = "floor44"
        Me.floor44.Size = New System.Drawing.Size(40, 40)
        Me.floor44.TabIndex = 87
        Me.floor44.TabStop = False
        '
        'floor43
        '
        Me.floor43.BackColor = System.Drawing.Color.Transparent
        Me.floor43.Location = New System.Drawing.Point(124, 288)
        Me.floor43.Name = "floor43"
        Me.floor43.Size = New System.Drawing.Size(40, 40)
        Me.floor43.TabIndex = 86
        Me.floor43.TabStop = False
        '
        'floor42
        '
        Me.floor42.BackColor = System.Drawing.Color.Transparent
        Me.floor42.Location = New System.Drawing.Point(67, 288)
        Me.floor42.Name = "floor42"
        Me.floor42.Size = New System.Drawing.Size(40, 40)
        Me.floor42.TabIndex = 85
        Me.floor42.TabStop = False
        '
        'floor41
        '
        Me.floor41.BackColor = System.Drawing.Color.Transparent
        Me.floor41.Location = New System.Drawing.Point(10, 288)
        Me.floor41.Name = "floor41"
        Me.floor41.Size = New System.Drawing.Size(40, 40)
        Me.floor41.TabIndex = 84
        Me.floor41.TabStop = False
        '
        'floor56
        '
        Me.floor56.BackColor = System.Drawing.Color.Transparent
        Me.floor56.Location = New System.Drawing.Point(401, 345)
        Me.floor56.Name = "floor56"
        Me.floor56.Size = New System.Drawing.Size(40, 40)
        Me.floor56.TabIndex = 99
        Me.floor56.TabStop = False
        '
        'floor55
        '
        Me.floor55.BackColor = System.Drawing.Color.Transparent
        Me.floor55.Location = New System.Drawing.Point(344, 345)
        Me.floor55.Name = "floor55"
        Me.floor55.Size = New System.Drawing.Size(40, 40)
        Me.floor55.TabIndex = 98
        Me.floor55.TabStop = False
        '
        'floor54
        '
        Me.floor54.BackColor = System.Drawing.Color.Transparent
        Me.floor54.Location = New System.Drawing.Point(287, 346)
        Me.floor54.Name = "floor54"
        Me.floor54.Size = New System.Drawing.Size(40, 40)
        Me.floor54.TabIndex = 97
        Me.floor54.TabStop = False
        '
        'floor53
        '
        Me.floor53.BackColor = System.Drawing.Color.Transparent
        Me.floor53.Location = New System.Drawing.Point(233, 345)
        Me.floor53.Name = "floor53"
        Me.floor53.Size = New System.Drawing.Size(40, 40)
        Me.floor53.TabIndex = 96
        Me.floor53.TabStop = False
        '
        'floor52
        '
        Me.floor52.BackColor = System.Drawing.Color.Transparent
        Me.floor52.Location = New System.Drawing.Point(179, 344)
        Me.floor52.Name = "floor52"
        Me.floor52.Size = New System.Drawing.Size(40, 40)
        Me.floor52.TabIndex = 95
        Me.floor52.TabStop = False
        '
        'floor51
        '
        Me.floor51.BackColor = System.Drawing.Color.Transparent
        Me.floor51.Location = New System.Drawing.Point(125, 344)
        Me.floor51.Name = "floor51"
        Me.floor51.Size = New System.Drawing.Size(40, 40)
        Me.floor51.TabIndex = 94
        Me.floor51.TabStop = False
        '
        'floor50
        '
        Me.floor50.BackColor = System.Drawing.Color.Transparent
        Me.floor50.Location = New System.Drawing.Point(67, 344)
        Me.floor50.Name = "floor50"
        Me.floor50.Size = New System.Drawing.Size(40, 40)
        Me.floor50.TabIndex = 93
        Me.floor50.TabStop = False
        '
        'floor49
        '
        Me.floor49.BackColor = System.Drawing.Color.Transparent
        Me.floor49.Location = New System.Drawing.Point(10, 344)
        Me.floor49.Name = "floor49"
        Me.floor49.Size = New System.Drawing.Size(40, 40)
        Me.floor49.TabIndex = 92
        Me.floor49.TabStop = False
        '
        'floor64
        '
        Me.floor64.BackColor = System.Drawing.Color.Transparent
        Me.floor64.Location = New System.Drawing.Point(402, 402)
        Me.floor64.Name = "floor64"
        Me.floor64.Size = New System.Drawing.Size(40, 40)
        Me.floor64.TabIndex = 107
        Me.floor64.TabStop = False
        '
        'floor63
        '
        Me.floor63.BackColor = System.Drawing.Color.Transparent
        Me.floor63.Location = New System.Drawing.Point(345, 401)
        Me.floor63.Name = "floor63"
        Me.floor63.Size = New System.Drawing.Size(40, 40)
        Me.floor63.TabIndex = 106
        Me.floor63.TabStop = False
        '
        'floor62
        '
        Me.floor62.BackColor = System.Drawing.Color.Transparent
        Me.floor62.Location = New System.Drawing.Point(287, 401)
        Me.floor62.Name = "floor62"
        Me.floor62.Size = New System.Drawing.Size(40, 40)
        Me.floor62.TabIndex = 105
        Me.floor62.TabStop = False
        '
        'floor61
        '
        Me.floor61.BackColor = System.Drawing.Color.Transparent
        Me.floor61.Location = New System.Drawing.Point(233, 401)
        Me.floor61.Name = "floor61"
        Me.floor61.Size = New System.Drawing.Size(40, 40)
        Me.floor61.TabIndex = 104
        Me.floor61.TabStop = False
        '
        'floor60
        '
        Me.floor60.BackColor = System.Drawing.Color.Transparent
        Me.floor60.Location = New System.Drawing.Point(180, 401)
        Me.floor60.Name = "floor60"
        Me.floor60.Size = New System.Drawing.Size(40, 40)
        Me.floor60.TabIndex = 103
        Me.floor60.TabStop = False
        '
        'floor59
        '
        Me.floor59.BackColor = System.Drawing.Color.Transparent
        Me.floor59.Location = New System.Drawing.Point(124, 401)
        Me.floor59.Name = "floor59"
        Me.floor59.Size = New System.Drawing.Size(40, 40)
        Me.floor59.TabIndex = 102
        Me.floor59.TabStop = False
        '
        'floor58
        '
        Me.floor58.BackColor = System.Drawing.Color.Transparent
        Me.floor58.Location = New System.Drawing.Point(67, 400)
        Me.floor58.Name = "floor58"
        Me.floor58.Size = New System.Drawing.Size(40, 40)
        Me.floor58.TabIndex = 101
        Me.floor58.TabStop = False
        '
        'floor57
        '
        Me.floor57.BackColor = System.Drawing.Color.Transparent
        Me.floor57.Location = New System.Drawing.Point(10, 401)
        Me.floor57.Name = "floor57"
        Me.floor57.Size = New System.Drawing.Size(40, 40)
        Me.floor57.TabIndex = 100
        Me.floor57.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Timer2
        '
        Me.Timer2.Interval = 1
        '
        'Timer3
        '
        Me.Timer3.Interval = 1
        '
        'Timer4
        '
        Me.Timer4.Interval = 1
        '
        'Timer5
        '
        Me.Timer5.Interval = 1
        '
        'Timer6
        '
        Me.Timer6.Interval = 1
        '
        'Timer7
        '
        Me.Timer7.Interval = 1
        '
        'Timer8
        '
        Me.Timer8.Interval = 1
        '
        'Timer9
        '
        Me.Timer9.Interval = 1
        '
        'Timer10
        '
        Me.Timer10.Interval = 1
        '
        'Timer11
        '
        Me.Timer11.Interval = 1
        '
        'Timer12
        '
        Me.Timer12.Interval = 1
        '
        'Timer13
        '
        Me.Timer13.Interval = 1
        '
        'Timer14
        '
        Me.Timer14.Interval = 1
        '
        'Timer15
        '
        Me.Timer15.Interval = 1
        '
        'Timer16
        '
        Me.Timer16.Interval = 1
        '
        'Timer17
        '
        Me.Timer17.Interval = 1
        '
        'Timer18
        '
        Me.Timer18.Interval = 1
        '
        'Timer19
        '
        Me.Timer19.Interval = 1
        '
        'Timer20
        '
        Me.Timer20.Interval = 1
        '
        'Timer21
        '
        Me.Timer21.Interval = 1
        '
        'Timer22
        '
        Me.Timer22.Interval = 1
        '
        'Timer23
        '
        Me.Timer23.Interval = 1
        '
        'Timer24
        '
        Me.Timer24.Interval = 1
        '
        'Timer25
        '
        Me.Timer25.Interval = 1
        '
        'Timer26
        '
        Me.Timer26.Interval = 1
        '
        'Timer27
        '
        Me.Timer27.Interval = 1
        '
        'Timer28
        '
        Me.Timer28.Interval = 1
        '
        'Timer29
        '
        Me.Timer29.Interval = 1
        '
        'Timer30
        '
        Me.Timer30.Interval = 1
        '
        'Timer31
        '
        Me.Timer31.Interval = 1
        '
        'Timer32
        '
        Me.Timer32.Interval = 1
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.Location = New System.Drawing.Point(132, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 23)
        Me.Button1.TabIndex = 108
        Me.Button1.Text = "White"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Location = New System.Drawing.Point(464, 67)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(210, 71)
        Me.GroupBox2.TabIndex = 109
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Transparent
        Me.Button2.Enabled = False
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(8, 42)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(196, 23)
        Me.Button2.TabIndex = 109
        Me.Button2.Text = "Start"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Transparent
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(578, 279)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(96, 23)
        Me.Button4.TabIndex = 110
        Me.Button4.Text = "New Game"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Transparent
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(578, 254)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(96, 23)
        Me.Button5.TabIndex = 111
        Me.Button5.Text = "Load Game --->"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Transparent
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(578, 229)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(96, 23)
        Me.Button6.TabIndex = 112
        Me.Button6.Text = "Save Game --->"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 20
        Me.ListBox2.Location = New System.Drawing.Point(699, 30)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(148, 302)
        Me.ListBox2.TabIndex = 4
        Me.ListBox2.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(695, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 20)
        Me.Label5.TabIndex = 113
        Me.Label5.Text = "Saves"
        Me.Label5.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(699, 338)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(148, 26)
        Me.TextBox2.TabIndex = 114
        Me.TextBox2.Text = "Chess Game 1"
        Me.TextBox2.Visible = False
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(699, 370)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(148, 37)
        Me.Button7.TabIndex = 115
        Me.Button7.Text = "Save Game"
        Me.Button7.UseVisualStyleBackColor = True
        Me.Button7.Visible = False
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(699, 413)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(148, 37)
        Me.Button8.TabIndex = 116
        Me.Button8.Text = "Remove Game"
        Me.Button8.UseVisualStyleBackColor = True
        Me.Button8.Visible = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Transparent
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(464, 163)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(62, 27)
        Me.Button9.TabIndex = 117
        Me.Button9.Text = "Change"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(532, 164)
        Me.TextBox3.MaxLength = 6
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(142, 26)
        Me.TextBox3.TabIndex = 118
        Me.TextBox3.Text = "Player"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.Transparent
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(464, 196)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(62, 27)
        Me.Button10.TabIndex = 119
        Me.Button10.Text = "Connect"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(532, 197)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(142, 26)
        Me.TextBox4.TabIndex = 120
        Me.TextBox4.Text = "127.0.0.1"
        '
        'Timer33
        '
        Me.Timer33.Interval = 1
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.Transparent
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.Location = New System.Drawing.Point(464, 229)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(108, 48)
        Me.Button11.TabIndex = 121
        Me.Button11.Text = "Create game"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Timer34
        '
        Me.Timer34.Interval = 1
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.Transparent
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.Location = New System.Drawing.Point(464, 279)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(108, 23)
        Me.Button12.TabIndex = 122
        Me.Button12.Text = "Disconnect"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(694, 472)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.soldier1_black)
        Me.Controls.Add(Me.soldier2_black)
        Me.Controls.Add(Me.soldier3_black)
        Me.Controls.Add(Me.soldier4_black)
        Me.Controls.Add(Me.soldier5_black)
        Me.Controls.Add(Me.soldier6_black)
        Me.Controls.Add(Me.soldier7_black)
        Me.Controls.Add(Me.soldier8_black)
        Me.Controls.Add(Me.rook1_black)
        Me.Controls.Add(Me.knight1_black)
        Me.Controls.Add(Me.bishop1_black)
        Me.Controls.Add(Me.king_black)
        Me.Controls.Add(Me.queen_black)
        Me.Controls.Add(Me.bishop2_black)
        Me.Controls.Add(Me.knight2_black)
        Me.Controls.Add(Me.rook2_black)
        Me.Controls.Add(Me.soldier1_white)
        Me.Controls.Add(Me.soldier2_white)
        Me.Controls.Add(Me.soldier3_white)
        Me.Controls.Add(Me.soldier4_white)
        Me.Controls.Add(Me.soldier5_white)
        Me.Controls.Add(Me.soldier6_white)
        Me.Controls.Add(Me.soldier7_white)
        Me.Controls.Add(Me.soldier8_white)
        Me.Controls.Add(Me.rook1_white)
        Me.Controls.Add(Me.knight1_white)
        Me.Controls.Add(Me.bishop1_white)
        Me.Controls.Add(Me.king_white)
        Me.Controls.Add(Me.queen_white)
        Me.Controls.Add(Me.bishop2_white)
        Me.Controls.Add(Me.knight2_white)
        Me.Controls.Add(Me.rook2_white)
        Me.Controls.Add(Me.floor64)
        Me.Controls.Add(Me.floor63)
        Me.Controls.Add(Me.floor62)
        Me.Controls.Add(Me.floor61)
        Me.Controls.Add(Me.floor60)
        Me.Controls.Add(Me.floor59)
        Me.Controls.Add(Me.floor58)
        Me.Controls.Add(Me.floor57)
        Me.Controls.Add(Me.floor56)
        Me.Controls.Add(Me.floor55)
        Me.Controls.Add(Me.floor54)
        Me.Controls.Add(Me.floor53)
        Me.Controls.Add(Me.floor52)
        Me.Controls.Add(Me.floor51)
        Me.Controls.Add(Me.floor50)
        Me.Controls.Add(Me.floor49)
        Me.Controls.Add(Me.floor48)
        Me.Controls.Add(Me.floor47)
        Me.Controls.Add(Me.floor46)
        Me.Controls.Add(Me.floor45)
        Me.Controls.Add(Me.floor44)
        Me.Controls.Add(Me.floor43)
        Me.Controls.Add(Me.floor42)
        Me.Controls.Add(Me.floor41)
        Me.Controls.Add(Me.floor40)
        Me.Controls.Add(Me.floor39)
        Me.Controls.Add(Me.floor38)
        Me.Controls.Add(Me.floor37)
        Me.Controls.Add(Me.floor36)
        Me.Controls.Add(Me.floor35)
        Me.Controls.Add(Me.floor34)
        Me.Controls.Add(Me.floor33)
        Me.Controls.Add(Me.floor32)
        Me.Controls.Add(Me.floor31)
        Me.Controls.Add(Me.floor30)
        Me.Controls.Add(Me.floor29)
        Me.Controls.Add(Me.floor28)
        Me.Controls.Add(Me.floor27)
        Me.Controls.Add(Me.floor26)
        Me.Controls.Add(Me.floor25)
        Me.Controls.Add(Me.floor24)
        Me.Controls.Add(Me.floor23)
        Me.Controls.Add(Me.floor22)
        Me.Controls.Add(Me.floor21)
        Me.Controls.Add(Me.floor20)
        Me.Controls.Add(Me.floor19)
        Me.Controls.Add(Me.floor18)
        Me.Controls.Add(Me.floor17)
        Me.Controls.Add(Me.floor16)
        Me.Controls.Add(Me.floor15)
        Me.Controls.Add(Me.floor14)
        Me.Controls.Add(Me.floor13)
        Me.Controls.Add(Me.floor12)
        Me.Controls.Add(Me.floor11)
        Me.Controls.Add(Me.floor10)
        Me.Controls.Add(Me.floor9)
        Me.Controls.Add(Me.floor8)
        Me.Controls.Add(Me.floor7)
        Me.Controls.Add(Me.floor6)
        Me.Controls.Add(Me.floor5)
        Me.Controls.Add(Me.floor4)
        Me.Controls.Add(Me.floor3)
        Me.Controls.Add(Me.floor2)
        Me.Controls.Add(Me.floor1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.location_indicator)
        Me.Controls.Add(Me.PictureBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MultiPlayer Chess Beta by  ----> kareem adel fathi <----"
        CType(Me.location_indicator, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.rook2_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.knight2_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bishop2_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.queen_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.king_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bishop1_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.knight1_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rook1_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier1_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier2_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier3_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier4_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier5_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier6_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier7_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier8_white, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rook1_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.knight1_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bishop1_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.king_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.queen_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bishop2_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.knight2_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rook2_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier1_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier2_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier3_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier4_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier5_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier6_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier7_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.soldier8_black, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.floor57, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents location_indicator As System.Windows.Forms.PictureBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents rook2_white As System.Windows.Forms.PictureBox
    Friend WithEvents knight2_white As System.Windows.Forms.PictureBox
    Friend WithEvents bishop2_white As System.Windows.Forms.PictureBox
    Friend WithEvents queen_white As System.Windows.Forms.PictureBox
    Friend WithEvents king_white As System.Windows.Forms.PictureBox
    Friend WithEvents bishop1_white As System.Windows.Forms.PictureBox
    Friend WithEvents knight1_white As System.Windows.Forms.PictureBox
    Friend WithEvents rook1_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier1_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier2_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier3_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier4_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier5_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier6_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier7_white As System.Windows.Forms.PictureBox
    Friend WithEvents soldier8_white As System.Windows.Forms.PictureBox
    Friend WithEvents rook1_black As System.Windows.Forms.PictureBox
    Friend WithEvents knight1_black As System.Windows.Forms.PictureBox
    Friend WithEvents bishop1_black As System.Windows.Forms.PictureBox
    Friend WithEvents king_black As System.Windows.Forms.PictureBox
    Friend WithEvents queen_black As System.Windows.Forms.PictureBox
    Friend WithEvents bishop2_black As System.Windows.Forms.PictureBox
    Friend WithEvents knight2_black As System.Windows.Forms.PictureBox
    Friend WithEvents rook2_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier1_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier2_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier3_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier4_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier5_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier6_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier7_black As System.Windows.Forms.PictureBox
    Friend WithEvents soldier8_black As System.Windows.Forms.PictureBox
    Friend WithEvents floor1 As System.Windows.Forms.PictureBox
    Friend WithEvents floor2 As System.Windows.Forms.PictureBox
    Friend WithEvents floor3 As System.Windows.Forms.PictureBox
    Friend WithEvents floor4 As System.Windows.Forms.PictureBox
    Friend WithEvents floor5 As System.Windows.Forms.PictureBox
    Friend WithEvents floor6 As System.Windows.Forms.PictureBox
    Friend WithEvents floor7 As System.Windows.Forms.PictureBox
    Friend WithEvents floor8 As System.Windows.Forms.PictureBox
    Friend WithEvents floor9 As System.Windows.Forms.PictureBox
    Friend WithEvents floor10 As System.Windows.Forms.PictureBox
    Friend WithEvents floor11 As System.Windows.Forms.PictureBox
    Friend WithEvents floor12 As System.Windows.Forms.PictureBox
    Friend WithEvents floor13 As System.Windows.Forms.PictureBox
    Friend WithEvents floor14 As System.Windows.Forms.PictureBox
    Friend WithEvents floor15 As System.Windows.Forms.PictureBox
    Friend WithEvents floor16 As System.Windows.Forms.PictureBox
    Friend WithEvents floor17 As System.Windows.Forms.PictureBox
    Friend WithEvents floor18 As System.Windows.Forms.PictureBox
    Friend WithEvents floor19 As System.Windows.Forms.PictureBox
    Friend WithEvents floor20 As System.Windows.Forms.PictureBox
    Friend WithEvents floor21 As System.Windows.Forms.PictureBox
    Friend WithEvents floor22 As System.Windows.Forms.PictureBox
    Friend WithEvents floor23 As System.Windows.Forms.PictureBox
    Friend WithEvents floor24 As System.Windows.Forms.PictureBox
    Friend WithEvents floor25 As System.Windows.Forms.PictureBox
    Friend WithEvents floor26 As System.Windows.Forms.PictureBox
    Friend WithEvents floor27 As System.Windows.Forms.PictureBox
    Friend WithEvents floor28 As System.Windows.Forms.PictureBox
    Friend WithEvents floor29 As System.Windows.Forms.PictureBox
    Friend WithEvents floor30 As System.Windows.Forms.PictureBox
    Friend WithEvents floor31 As System.Windows.Forms.PictureBox
    Friend WithEvents floor32 As System.Windows.Forms.PictureBox
    Friend WithEvents floor33 As System.Windows.Forms.PictureBox
    Friend WithEvents floor34 As System.Windows.Forms.PictureBox
    Friend WithEvents floor35 As System.Windows.Forms.PictureBox
    Friend WithEvents floor36 As System.Windows.Forms.PictureBox
    Friend WithEvents floor37 As System.Windows.Forms.PictureBox
    Friend WithEvents floor38 As System.Windows.Forms.PictureBox
    Friend WithEvents floor39 As System.Windows.Forms.PictureBox
    Friend WithEvents floor40 As System.Windows.Forms.PictureBox
    Friend WithEvents floor48 As System.Windows.Forms.PictureBox
    Friend WithEvents floor47 As System.Windows.Forms.PictureBox
    Friend WithEvents floor46 As System.Windows.Forms.PictureBox
    Friend WithEvents floor45 As System.Windows.Forms.PictureBox
    Friend WithEvents floor44 As System.Windows.Forms.PictureBox
    Friend WithEvents floor43 As System.Windows.Forms.PictureBox
    Friend WithEvents floor42 As System.Windows.Forms.PictureBox
    Friend WithEvents floor41 As System.Windows.Forms.PictureBox
    Friend WithEvents floor56 As System.Windows.Forms.PictureBox
    Friend WithEvents floor55 As System.Windows.Forms.PictureBox
    Friend WithEvents floor54 As System.Windows.Forms.PictureBox
    Friend WithEvents floor53 As System.Windows.Forms.PictureBox
    Friend WithEvents floor52 As System.Windows.Forms.PictureBox
    Friend WithEvents floor51 As System.Windows.Forms.PictureBox
    Friend WithEvents floor50 As System.Windows.Forms.PictureBox
    Friend WithEvents floor49 As System.Windows.Forms.PictureBox
    Friend WithEvents floor64 As System.Windows.Forms.PictureBox
    Friend WithEvents floor63 As System.Windows.Forms.PictureBox
    Friend WithEvents floor62 As System.Windows.Forms.PictureBox
    Friend WithEvents floor61 As System.Windows.Forms.PictureBox
    Friend WithEvents floor60 As System.Windows.Forms.PictureBox
    Friend WithEvents floor59 As System.Windows.Forms.PictureBox
    Friend WithEvents floor58 As System.Windows.Forms.PictureBox
    Friend WithEvents floor57 As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Timer4 As System.Windows.Forms.Timer
    Friend WithEvents Timer5 As System.Windows.Forms.Timer
    Friend WithEvents Timer6 As System.Windows.Forms.Timer
    Friend WithEvents Timer7 As System.Windows.Forms.Timer
    Friend WithEvents Timer8 As System.Windows.Forms.Timer
    Friend WithEvents Timer9 As System.Windows.Forms.Timer
    Friend WithEvents Timer10 As System.Windows.Forms.Timer
    Friend WithEvents Timer11 As System.Windows.Forms.Timer
    Friend WithEvents Timer12 As System.Windows.Forms.Timer
    Friend WithEvents Timer13 As System.Windows.Forms.Timer
    Friend WithEvents Timer14 As System.Windows.Forms.Timer
    Friend WithEvents Timer15 As System.Windows.Forms.Timer
    Friend WithEvents Timer16 As System.Windows.Forms.Timer
    Friend WithEvents Timer17 As System.Windows.Forms.Timer
    Friend WithEvents Timer18 As System.Windows.Forms.Timer
    Friend WithEvents Timer19 As System.Windows.Forms.Timer
    Friend WithEvents Timer20 As System.Windows.Forms.Timer
    Friend WithEvents Timer21 As System.Windows.Forms.Timer
    Friend WithEvents Timer22 As System.Windows.Forms.Timer
    Friend WithEvents Timer23 As System.Windows.Forms.Timer
    Friend WithEvents Timer24 As System.Windows.Forms.Timer
    Friend WithEvents Timer25 As System.Windows.Forms.Timer
    Friend WithEvents Timer26 As System.Windows.Forms.Timer
    Friend WithEvents Timer27 As System.Windows.Forms.Timer
    Friend WithEvents Timer28 As System.Windows.Forms.Timer
    Friend WithEvents Timer29 As System.Windows.Forms.Timer
    Friend WithEvents Timer30 As System.Windows.Forms.Timer
    Friend WithEvents Timer31 As System.Windows.Forms.Timer
    Friend WithEvents Timer32 As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Timer33 As System.Windows.Forms.Timer
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Timer34 As System.Windows.Forms.Timer
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Button12 As System.Windows.Forms.Button

End Class
